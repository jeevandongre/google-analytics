class RegistrationController < ApplicationController
  
class Exits
  extend Garb::Model
#specifiy the metrics and dimensions according to your requirement 
#visit http://code.google.com/apis/analytics/docs/gdata/dimsmets/dimsmets.html
#for more information
  metrics :visits, :percentNewVisits, :organicSearches, :bounces, :entranceBounceRate, :avgTimeOnPage
  dimensions :visitorType,:source, :day,:month
  
end
def registration
  require 'csv'
#specifiy the name of the of the file  
  filename="reports.csv"
Garb::Session.login("email@gmail.com", "password")
profile = Garb::Management::Profile.all.detect {|p| p.web_property_id == 'UA-xx-xxxxxxx'} 
#uncomment below if want to fetch the results between dates
#@report = Exits.results(profile, {:start_date => Time.now - 300000, :end_date => Time.now})
@report = Exits.results(profile, :metrics =>[:visits])
  

#reports = Exits.results(profile, :filters => {:start_date => Time.now - (200000),:end_date => Time.now})
  CSV.open("public/export/#{filename}", "wb") do |csv|
  
  #change the header of the csv file according to your requirement
          csv <<["visits","percent_new_visits","organic_searches","bounces","entrance_bounce_rate","avg_time_on_page","visitor_type","source"]
      @report.each do |rep|
        #fetch the data from the results
           csv << [rep.visits, rep.percent_new_visits, rep.organic_searches, rep.bounces, rep.entrance_bounce_rate, rep.avg_time_on_page,rep.visitor_type,rep.source]
           
 send_file("public/export/#{filename}"  ,:filename => "/cs.csv" ,
            :type => "csv",
            :disposition => 'attachment')
 end 
 end
 end
end
  
